<?php
namespace Drupal\drupal_simple_form\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
/**
 * Our simple form class.
 */
class SimpleForm extends FormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'drupal_simple_form';
  }
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
   // $form['username'] = [
   //   '#type' => 'email',
   //   '#title' => $this->t('email'),
   // ];
  //$form['password'] = [
    //  '#type' => 'textfield',
    //  '#title' => $this->t('Password'),
  //  ];
  //  $form['submit'] = [
    //  '#type' => 'submit',
    //  '#value' => $this->t('Submit'),
  //  ];
  $form['first_name'] = array(
    '#type' => 'textfield',
    '#title' => t('First Name:'),
    '#required' => TRUE,
  );
  $form['last_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Last Name:'),
    '#required' => TRUE,
  );
  $form['email'] = array(
    '#type' => 'email',
    '#title' => t('Email ID:'),
    '#required' => TRUE,
  );
  $form['candidate_number'] = array (
    '#type' => 'tel',
    '#title' => t('Mobile no'),
  );
  $form['candidate_dob'] = array (
    '#type' => 'date',
    '#title' => t('DOB'),
    '#required' => TRUE,
  );
  $form['candidate_gender'] = array (
    '#type' => 'select',
    '#title' => ('Gender'),
    '#options' => array(
      'Female' => t('Female'),
      'male' => t('Male'),
    ),
  );
  $form['candidate_confirmation'] = array (
    '#type' => 'radios',
    '#title' => ('Are you above 18 years old?'),
    '#options' => array(
      'Yes' =>t('Yes'),
      'No' =>t('No')
    ),
  );
  $form['Address1'] = array (
    '#type' => 'textfield',
    '#title' => ('Address Line1'),
    '#required' => TRUE,
  );
  $form['Address'] = array (
    '#type' => 'textfield',
    '#title' => ('State'),
    '#required' => TRUE,
  );
  $form['actions']['#type'] = 'actions';
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => $this->t('Submit'),
    '#button_type' => 'primary',
  );
    return $form;
  }
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    drupal_set_message($this->t('@can_name ,Your application is being submitted!', array('@can_name' => $form_state->getValue('candidate_name'))));
     foreach ($form_state->getValues() as $key => $value) {
       drupal_set_message($key . ': ' . $value);
     }
    }
  }
?>