<?php
/**
 * @file
 * Contains \Drupal\ModuleNameBlock\Plugin\Block\ArticleBlock.
 */
namespace Drupal\module_name\Plugin\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
/**
 * Provides a 'Custom' block.
 *
 * @Block(
 *   id = "module_name",
 *   admin_label = @Translation("ovulation predictor calculator"),
 *   category = @Translation("Custom Custom block example")
 * )
 */
class ModuleNameBlock extends BlockBase {
/**
 * {@inheritdoc}
 */

public function build() {
  return [
    '#theme' => 'module_name_block',
    '#attached' => [
      'library' => 'module_name/module-name-app',
    ],
  ];
}
}